<?php

namespace Drupal\restfullogger\Plugin\rest\resource;

use Drupal\Component\Utility\Html;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides a resource to post watchdog database logs.
 *
 * @RestResource(
 *   id = "restfullogger",
 *   label = @Translation("Watchdog database logger"),
 *   uri_paths = {
 *     "create" = "/dblog/logger"
 *   }
 * )
 */
class RestfulLogger extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new DefaultRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('restfullogger'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to a POST request and logs the message.
   *
   * @param mixed $data
   *   The data to be logged.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data): ModifiedResourceResponse {
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('post log messages')) {
      throw new AccessDeniedHttpException();
    }
    // Validate that a message is there.
    $message = $this->validateAndParseMessage($data);
    $channel = empty($data['channel']) ? 'restfullogger' : $data['channel'];
    $severity = $this->validateAndParseSeverity($data);
    $path = $this->validateAndParsePath($data);

    // Log the message using the logger context.
    $this->logger->log($severity, '@message, path:  @path', [
      '@message' => $message,
      '@path' => $path,
    ]);

    // Build our response.
    $response = [
      'message' => 'successfully logged',
      'dblog_message' => $this->t('@message, path:  @path', [
        '@message' => $message,
        '@path' => $path,
      ]),
      'dblog_channel' => $channel,
      'dblog_severity' => $severity,
    ];

    return new ModifiedResourceResponse($response, 200);
  }

  /**
   * Validates and extracts the message to log.
   *
   * @param array $request
   *   The data array with the message value.
   *
   * @return resource|string
   *   The value for the message.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   Thrown when the request is mal-formatted.
   */
  private function validateAndParseMessage(array $request): string {
    // Check if the message value exists.
    if (empty($request['message'])) {
      throw new BadRequestHttpException('The message value is missing');
    }
    return Html::escape($request['message']);
  }

  /**
   * Validates and extracts the severity of the message.
   *
   * @param array $request
   *   The data array with the severity value.
   *
   * @return string
   *   The value for the severity.
   */
  private function validateAndParseSeverity(array $request): string {
    $levels = RfcLogLevel::getLevels();
    $severity = empty($request['severity']) ? LogLevel::NOTICE : strtolower($request['severity']);
    // Default 'Notice' if the level provided is not available.
    return in_array(ucfirst($severity), $levels) ? $severity : LogLevel::NOTICE;
  }

  /**
   * Validates and extracts the path from the request.
   *
   * @param array $request
   *   The request array that contains the path value.
   *
   * @return resource|string
   *   The value for the path.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   Thrown when the request is mal-formatted.
   */
  private function validateAndParsePath(array $request): string {
    // Check if the path value exists.
    if (empty($request['path'])) {
      throw new BadRequestHttpException('The path value is missing');
    }
    return Html::escape($request['path']);
  }

}
