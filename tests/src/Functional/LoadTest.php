<?php

namespace Drupal\Tests\restfullogger\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure the logger works.
 *
 * @group restfullogger
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['restfullogger'];

  /**
   * Posts a log message and retrieves it via the REST API.
   */
  public function testPost() {
    $user = $this->drupalCreateUser([
      'restful post restfullogger',
      'post log messages',
    ]);
    $this->drupalLogin($user);
    $client = $this->getHttpClient();
    $url = Url::fromUserInput('/dblog/logger')
      ->setOption('query', ['_format' => 'json'])
      ->setAbsolute()
      ->toString();
    $token = $this->drupalGet("/session/token", [
      'query' => [
        '_format' => 'hal_json',
      ],
    ]);
    $body = $this->container->get('serializer')->serialize([
      'message' => 'Test message',
      'path' => '/',
      'severity' => 'notice',
    ], 'json');
    $request_options = [
      'cookies' => $this->getSessionCookies(),
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'X-CSRF-Token' => $token,
      ],
      'http_errors' => FALSE,
      'body' => $body,
    ];

    $response = $client->request('POST', $url, $request_options);
    $log = Json::decode((string) $response->getBody());
    $this->assertEquals(200, $response->getStatusCode(), sprintf('Status code equals %d incorrect with reason: "%s" and body: "%s".', $response->getStatusCode(), $response->getReasonPhrase(), (string) $response->getBody()));
    $this->assertEquals('successfully logged', $log['message'], sprintf('Message text is not correct, it is %s.', $log['message']));
    $this->assertEquals('Test message, path:  /', $log['dblog_message'], sprintf('dblog_message text is not correct, it is %s.', $log['dblog_message']));
    $this->assertEquals('restfullogger', $log['dblog_channel'], sprintf('dblog_channel value is not correct, it is %s.', $log['dblog_channel']));
    $this->assertEquals('notice', $log['dblog_severity'], sprintf('dblog_severity text is not correct, it is %s.', $log['dblog_severity']));
  }

}
