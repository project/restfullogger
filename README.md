# RESTful Logger

This module provides a REST endpoint for logging messages to Drupal's core
logger using JSON with a `message` value, `path`, `severity` and `channel` 
value as documented on the 
[Drupal Logging API documentation](https://www.drupal.org/docs/8/api/logging-api/overview).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Example JSON POST Body
- Troubleshooting
- Maintainers

## Requirements

This module requires the following core modules:

- [dblog](https://www.drupal.org/docs/8/core/modules/dblog)
- [rest](https://www.drupal.org/docs/8/core/modules/rest/)

## Recommended modules

[REST UI module](https://www.drupal.org/project/restui): When enabled,
it displays active REST endpoints at Configuration > Services > REST.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend.
2. Grant the desired permissions needed to the `POST log messages`
permission and the `Access POST on Watchdog database logger resource` permission
via the People > Permissions page.

## Example JSON POST Body

### Required Values:
- `message`
- `path`

### Default Values:
- The default value for `channel` is `restfulloger`.
- The default value for `severity` is `Notice`.

```json
{
  "message": "The message you want to log.",
  "severity": "Error",
  "channel": "module_machine_name",
  "path": "/path/to/page"
}
```

## Troubleshooting

If the logger does not get recorded when POSTing to `/dblog/logger`

- Ensure the `POST log messages` and `Access POST on Watchdog database logger 
resource` permissions are set to the correct roles on the People > Permissions 
page.
- Ensure the REST endpoint is enabled. (This is easier to check using the 
[REST UI module](https://www.drupal.org/project/restui).)
- Ensure your JSON body is formatted correctly and the `message` and `path`
values are defined.

## Maintainers

- Ryan McVeigh - [rymcveigh](https://www.drupal.org/u/rymcveigh)
